
@extends('modif')

@section('content')

	<form action="{{route('postmodifS',['id'=>$m['id']])}}" method="POST">
			
		{{csrf_field()}}
		<h4 class="bordeaux">Modifier une formation</h4>
		<div class="ligne"></div>
	 	<p>Nom : <input class="form-control" type="text" name="nom" value="{{$m->Nom}}" id="nom"/></p>
	 	<div class="ligne"></div>
	 	<p>Début : <input class="form-control" type="text" name="debut" value="{{$m->Debut}}" id="debut"/></p>
	 	<div class="ligne"></div>
	 	<p>Fin : <input class="form-control" type="text" name="fin" value="{{$m->Fin}}" id="fin"/></p>
	 	<div class="ligne"></div>
	 	<p>Information : <textarea class="form-control" type="text" name="info" id="info" style="height: 200px">{{$m->Info}}</textarea></p>
	 	<div class="ligne"></div>
	 	<p><input type="submit" value="Modifier" class="btn btn-light" style="color: maroon; border: 1px maroon solid;margin-top: 0px;"></input></p>
	</form>

@stop