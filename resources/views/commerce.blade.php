@extends("template")
@section("content")


<h2 class="bordeaux">Métiers du Commerce, du Management & de la Communication</h2>
<div class="ligne"></div>
<div style="font-size: 18px;">Voici les formations en rapport avec les métiers du commerce, du management et de la communication proposées au lycée Pasteur Mont Roland de Dole.</div>
</br>


<table class="table">
	<thead class="thead-light">
		<tr>
			<th>Nom</th>
			<th>Début</th>
			<th>Fin</th>
			<th>Information</th>
			<th>Contact</th>
			<th></th>
		</tr>
	</thead>
@foreach($tab as $ligne)
	<tr>
		<td>{{$ligne["Nom"]}}</td>
        <td>{{$ligne["Debut"]}}</td>
        <td>{{$ligne["Fin"]}}</td>
        <td>{{$ligne["Info"]}}</td>
        <td>
        	<a href="ContactDo"><button class="btn btn-light" style="color: maroon; border: 1px maroon solid;margin-top: 0px">Contact</button></a></td>
        <td>
        <a href="{{route('Validinfo',['id'=>$ligne['id']])}}"><button class="btn btn-light" style="color: maroon; border: 1px maroon solid;margin-top: 0px">Choisir cette formation</button></a></td>
	</tr>
@endforeach
</table>
@stop 