@extends("admin")
@section("content")


<h2 class="bordeaux">Métiers du Design & des Art</h2>
<div class="ligne"></div>
<div style="font-size: 18px;">Voici les formations en rapport avec les métiers du design et des art proposées au lycée Pasteur Mont Roland de Dole.</div>
</br>
<table class="table">
	<thead class="thead-light">
		<tr>
			<th>Nom</th>
			<th>Début</th>
			<th>Fin</th>
			<th>Information</th>
			<th>Option</th>
			<th></th>
		</tr>
	</thead>
@foreach($tab as $ligne)
	<tr>
		<td>{{$ligne["Nom"]}}</td>
        <td>{{$ligne["Debut"]}}</td>
        <td>{{$ligne["Fin"]}}</td>
        <td>{{$ligne["Info"]}}</td>
        <td><a href="{{route('modifA',['id'=>$ligne['id']])}}"><button class="btn btn-light" style="color: maroon; border: 1px maroon solid;margin-top: 0px">Modifier</button></a>
		<a href="{{route('deleteA',['id'=>$ligne['id']])}}"><button class="btn btn-light" style="color: maroon; border: 1px maroon solid;margin-top: 0px">Supprimer</button></a></td>
	</tr>
@endforeach
</table>
</br>
<a href="{{route('createA')}}"><button class="btn btn-light" style="color: maroon; border: 1px maroon solid;margin-top: 0px">Ajouter une formation</button></a>


@stop 