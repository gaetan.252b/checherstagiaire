@extends("admin")
@section("content")

 <h4 class="bordeaux" style="text-align: center;color: orange">Formation</h4></br>
 <div class="row mb-2">
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              
              <h3 class="mb-0">
                <a class="text-dark" href="{{route('industriesAdmin')}}">Industries & Nouvelles technologies</a>
              </h3>
              <p class="card-text mb-auto">Secteur avec des formations permettant un stage ayant un rapport avec l'informatique.</p>

            </div>
            <img class="card-img-right flex-auto d-none d-md-block" src="images/info.jpg" height="150" width="150"alt="Card image cap">
          </div>
        </div>
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <h3 class="mb-0">
                <a class="text-dark" href="{{route('santeAdmin')}}">Santé & Social</a>
              </h3>
              <p class="card-text mb-auto">Les formations pour les métiers de la santé et du social.</p>

            </div>
            <img class="card-img-right flex-auto d-none d-md-block" src="images/sante.jfif" height="170" width="150"alt="Card image cap">
          </div>
        </div>
      </div>

    <div class="row mb-2">
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              
              <h3 class="mb-0">
                <a class="text-dark" href="{{route('artAdmin')}}">Art & Design</a>
              </h3>
              <p class="card-text mb-auto">Secteur formant aux métiers artistiques et designer.</p>

            </div>
            <img class="card-img-right flex-auto d-none d-md-block" src="images/art.png" height="130" width="120"alt="Card image cap">
          </div>
        </div>
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <h3 class="mb-0">
                <a class="text-dark" href="{{route('commerceAdmin')}}">Commerce, Management & Communication</a>
              </h3>
              <p class="card-text mb-auto">Secteur des formations d'interactions aux personnes.</p>

            </div>
            <img class="card-img-right flex-auto d-none d-md-block" src="images/commu.png" height="120" width="120"alt="Card image cap">
          </div>
        </div>
      </div>

<div class="row mb-2">
        <div class="col-md-6">
<div class="ligne"></div>
<h4 class="bordeaux" style="text-align: center; color: red">Plan d'accès</h4>
      <div >
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1615.0667047266852!2d5.492268139064148!3d47.09500066071939!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478d4c97310a17a3%3A0x4edeebd526881fcb!2sLyc%C3%A9e%20Pasteur%20Mont%20Roland%20site%20Pasteur!5e0!3m2!1sfr!2sfr!4v1586284943454!5m2!1sfr!2sfr" width="450" height="450" frameborder="0" style="border:0;text-align: center;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </iframe>
      </div>

      </div>

<div class="col-md-6">
  <div class="ligne"></div>
    <h4 class="bordeaux" style="text-align: center; color: teal">Informations</h4></br>
</br>
</br>
      <div class="" style="text-align: center;font-size: 25px;"><strong>Site Mont-Roland</strong></br>55 Boulevard Wilson 39100 Dole</br>03 94 79 66 00</div> </br></br>
</br>
      <div class="" style="text-align: center;font-size: 25px;"><strong>Site Pasteur</strong></br>9 Avenue Rockfeller 39100 Dole</br>03 94 79 66 00</div>
</div></div>
<div class="ligne"></div>
<h4 class="bordeaux" style="text-align: center; color: green">Contact</h4>
</br>
<form method='post' action="{{route('message')}}">
    {{csrf_field()}}

        <div class="form-group">
            <label><strong>Email*</strong></label>
            <input type="text" class="form-control" name="email" required value="<?php echo (isset($_POST['email'])) ? $expediteur : '' ?>">
        </div>

        <div>
          <label for="Message"><strong>Message*</strong></label>
          <textarea placeholder="" class="form-control" name="message" required><?php echo (isset($_POST['message'])) ? $message : '' ?></textarea>
        </div>
      </br>
       <div class="element">
                <button type="submit" name="envoye" value="Envoyer" class="btn btn-light" data-dismiss="modal" style="color: maroon; border: 1px maroon solid;margin-top: 0px">Envoyer</button>
            </div>
    
      </form>

    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>

@stop 