
@extends('modif')

@section('content')

	<form action="{{route('postmodifTS',$m->id)}}" method="POST">
			
		{{csrf_field()}}
	<h2 class="bordeaux">Information</h2>
	<div class="ligne"></div>
	<label><strong>Merci de remplir le formulaire ci-dessous afin que nous puissions traiter votre demande dans les meilleurs délais</strong></label>

	<div>
     <label for="email">Qu'elle classe voulez-vous ?</label><br />
          <select class="form-control">
           <option disabled>-Métiers de l'Industries & des Nouvelles Technologies-</option>
           <option value="SIO@gmail.com">BTS SIO</option>
           <option disabled>-Métiers de la Santé & du Social-</option>
           <option value="ESF@gmail.com">BTS ESF</option>
           <option value="SP3S@gmail.com">BTS SP3S</option>
           <option disabled>-Métiers du Design & des Art-</option>
           <option value="MADE@gmail.com">DN MADE Spectacle - Costumes de scènes</option>
           <option value="DTMS@gmail.com">DTMS option Techniques de l'Habillage</option>
           <option disabled>-Métiers du Commerce, du Management & de la Communication-</option>
           <option value="SAM@gmail.com">BTS SAM</option>
           <option value="SCG@gmail.com">BTS CG</option>
       </select> 
    </div>
    <div class="form-group">
            <label>Nom de votre entreprise</label>
            <input type="text"class="form-control" name="entreprise" id="entreprise">
        </div>
        <div class="form-group">
            <label>Adresse</label>
            <input type="text"class="form-control" name="adresse" id="adresse">
        </div>
        <div class="form-group">
            <label>Téléphone</label>
            <input type="text"class="form-control" name="telephone" id="telephone">
        </div>
        <div class="form-group">
            <label>Nom du tuteur</label>
            <input type="text"class="form-control" name="nom" id="nom">
        </div>
        </br>
       <p><input type="submit" value="Envoyer" class="btn btn-light" style="color: maroon; border: 1px maroon solid;margin-top: 0px;"></input></p>



</form>
@stop 