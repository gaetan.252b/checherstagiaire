@extends("template")
@section("content")
<!--
<h4 class="bordeaux">Contacter le professeur principal de cette classe</h4>
<div class="ligne">
<label><strong>Merci de remplir le formulaire ci-dessous afin que nous puissions traiter votre demande dans les meilleurs délais</strong></label>
</div>
<form method='post' action="{{route('messageDo')}}">
  <div id="conteneur" style="display: flex;">
    <div class="element" style="width: 325px; margin-right: 50px">
        <div class="form-group">
            <label>Votre email</label>
            <input type="text" class="form-control">
        </div>
        
         <label for="email">Qu'elle classe voulez-vous contacter ?</label><br />
          <select name="email" id="email">
           <option disabled>-Métiers de l'Industries & des Nouvelles Technologies-</option>
           <option value="SIO@gmail.com">BTS SIO</option>
           <option disabled>-Métiers de la Santé & du Social-</option>
           <option value="ESF@gmail.com">BTS ESF</option>
           <option value="SP3S@gmail.com">BTS SP3S</option>
           <option disabled>-Métiers du Design & des Art-</option>
           <option value="MADE@gmail.com">DN MADE Spectacle - Costumes de scènes</option>
           <option value="DTMS@gmail.com">DTMS option Techniques de l'Habillage</option>
           <option disabled>-Métiers du Commerce, du Management & de la Communication-</option>
           <option value="SAM@gmail.com">BTS SAM</option>
           <option value="SCG@gmail.com">BTS CG</option>
       </select> 
  </div>

            <div class="modal-footer"></div>
          <label for="Message">Message</label>
          <textarea placeholder="" class="form-control"></textarea>
        </div>

        <div class="element">
                <button type="submit" name="envoye" value="Envoyer" class="btn btn-light" data-dismiss="modal" style="color: maroon; border: 1px maroon solid;margin-top: 0px">Envoyer</button>
            </div>

          </form>-->

<h4 class="bordeaux">Contacter notre lycée</h4>
<div class="ligne"></div>
<label><strong>Merci de remplir le formulaire ci-dessous afin que nous puissions traiter votre demande dans les meilleurs délais</strong></label>
<form method='post' action="{{route('messageDo')}}">
    {{csrf_field()}}
  <div id="conteneur" style="display: flex;">
    <div class="element" style=" margin-right: 50px">
        
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" required value="<?php echo (isset($_POST['email'])) ? $expediteur : '' ?>">
        </div>
    
<div>
     <label for="email">Qu'elle classe voulez-vous contacter ?</label><br />
          <select name="email" id="email"class="form-control">
           <option disabled>-Métiers de l'Industries & des Nouvelles Technologies-</option>
           <option value="SIO@gmail.com">BTS SIO</option>
           <option disabled>-Métiers de la Santé & du Social-</option>
           <option value="ESF@gmail.com">BTS ESF</option>
           <option value="SP3S@gmail.com">BTS SP3S</option>
           <option disabled>-Métiers du Design & des Art-</option>
           <option value="MADE@gmail.com">DN MADE Spectacle - Costumes de scènes</option>
           <option value="DTMS@gmail.com">DTMS option Techniques de l'Habillage</option>
           <option disabled>-Métiers du Commerce, du Management & de la Communication-</option>
           <option value="SAM@gmail.com">BTS SAM</option>
           <option value="SCG@gmail.com">BTS CG</option>
       </select> 
    </div>
    </br>
        <div>
          <label for="Message">Message</label>
          <textarea placeholder="" class="form-control" name="message" required><?php echo (isset($_POST['message'])) ? $message : '' ?></textarea>
        </div>
        </br>
       <div class="element">
                <button type="submit" name="envoye" value="Envoyer" class="btn btn-light" data-dismiss="modal" style="color: maroon; border: 1px maroon solid;margin-top: 0px">Envoyer</button>
            </div>
    </div>
      </form>


@stop 