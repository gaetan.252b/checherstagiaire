<!DOCTYPE html>
<html lang="en" style="height: 100%;">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">


  <!-- Bootstrap core CSS -->
  <link href="bootstrapv/css/bootstrap.css" rel="stylesheet">
  <link href="bootstrapv/css/a.css" rel="stylesheet">
      <script src="https://www.google.com/recaptcha/api.js" async defer></script>
      <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
</script>
<script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : '6LchO84UAAAAANlfBbEVRYMCRF-iYWGPTTScE4W5'
        });
      };
    </script>
<?php     
    $connection = new PDO('mysql:host=localhost;dbname=stagiaire;charset=utf8', 'root', '');
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



?> 

<?php
      // On recupere l'URL de la page pour ensuite affecter class = "active" aux liens de nav
      $page = $_SERVER['REQUEST_URI'];
      $page = str_replace("/checherstagiaire/public/", "",$page);
?>

<style>
  #conteneures
{
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
}

.textes{
  padding-top: 15px;padding-bottom: 10px;width: 1100px; margin: auto;text-align: justify;
}
.doré {
  background-color: #CCAB71;
}
.jaune {
  background-color: #60FE41;
}


/* Paragraphes en bleu par défaut */

/* Nouvelles règles si la fenêtre fait au plus 700px de large */
@media screen and (max-height: 700px)
{
    .footeur
    {
        visibility: hidden;
    }
    .footers
    {
        visibility: visible;
    }
}

</style>

</head>



<body style="background-color: rgba(248,250,255,0.88)">
<main role="main">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="first-slide" src="images/informatique2.jpg" height="400" width="1920" alt="First slide">
            <div class="carousel-caption d-none d-md-block">
  
            </div>
          </div>
          <div class="carousel-item">
            <img class="second-slide" src="images/etudiant2.jpg" height="400" width="1920" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">

          </div>
          </div>

        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </main>
  <!-- Navigation -->
<table >
  <nav class="navbar navbar-expand-lg navbar-light doré" id="nav">
    <div class="container">
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto container">
          <li <?php if($page == "accueil"){echo 'class="active nav-item "';} ?>
            <?php if($page ==""){echo 'class="active nav-item "';} ?>>
            <a class="nav-link" href="accueil">Accueil</a>
          </li>
          <li <?php if($page == "industries"){echo 'class="active nav-item"';} ?>>
            <a class="nav-link" href="industries">Industrie & Nouvelles technologies</a>
          </li>
          <li <?php if($page == "sante"){echo 'class="active nav-item"';} ?>>
            <a class="nav-link" href="sante">Santé & Social</a>
          </li>
          <li <?php if($page == "art"){echo 'class="active nav-item"';} ?>>
            <a class="nav-link" href="art">Art & Design</a>
          </li>
          <li <?php if($page == "commerce"){echo 'class="active nav-item"';} ?>>
            <a class="nav-link" href="commerce">Commerce, Management, Communication</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</table>

<div   class="texte" style="display: flex; justify-content: center;flex-wrap: wrap;">

    <div class="hui" style=" width: 1000px;">


        @yield("content")


    </div>

</div>

<!-- FOOTER -->

  <div style=" margin-top: 30px; ">

    <footer class="footer mt-auto" >
      <div id="conteneures"  class="textes" style="display: flex;">
      <div class="elements" >
         <p >© 2020 Lycée Pasteur Mont Roland - Tous droits réservés.</p>

       </div>
       <div class="elements" style=" text-align: right;">
          
          <a href="login" style="color: maroon; text-align: right;">Connexion</a>
      </div>
    </div>
    </footer>


  </div>

</body>

  <!-- Bootstrap core JavaScript -->
  <script src="bootstrap/js/bootstrap.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

   <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
  </body>
</html>
  <script src="bootstrap/js/bootstrap.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>


<!--  <script>
(function($){
    $(document).ready(function(){
        var offset = $(".navbar").offset().top;
        $(document).scroll(function(){
            var scrollTop = $(document).scrollTop();
            if(scrollTop > offset){
                $(".navbar").css("position", "fixed");
                $(".navbar").css("width", "100%");
                $(".navbar").css("margin-top", "-400px");
                $(".navbar").css("padding-left", "0px");
                $(".navbar").css("padding-right", "0px");

                $(".hui").css("margin-top", "55px");
            }
            else {
                $(".navbar").css("position", "static");

                $(".navbar").css("margin-top", "00px");
                $(".navbar").css("padding-left", "0px");
                $(".navbar").css("padding-right", "0px");

                $(".hui").css("margin-top", "0px");
            }
        });
    });
})(jQuery);


</script>-->

<script>
  (function() {
    var nav = document.getElementById('nav'),
        anchor = nav.getElementsByTagName('a'),
        current = window.location.pathname.split('/')[1];
        for (var i = 0; i < anchor.length; i++) {
        if(anchor[i].href == current) {
            anchor[i].className = "active";
        }
    }
})();
</script>