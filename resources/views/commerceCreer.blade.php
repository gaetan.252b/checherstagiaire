
@extends('modif')

@section('content')

	<form action="{{route('validC')}}" method="POST">
			
		{{csrf_field()}}
		<h4 class="bordeaux">Ajouter une formation</h4>
		<div class="ligne"></div>
	 	<p>Nom : <input class="form-control" type="text" name="nom" id="nom"/></p>
	 	<div class="ligne"></div>
	 	<p>Début : <input class="form-control" type="text" name="debut" id="debut"/></p>
	 	<div class="ligne"></div>
	 	<p>Fin : <input class="form-control" type="text" name="fin" id="fin"/></p>
	 	<div class="ligne"></div>
	 	<p>Information : <textarea class="form-control" type="text" name="info" id="info" style="height: 200px"></textarea></p>
	 	<div class="ligne"></div>
	 	<p><input type="submit" value="Ajouter" class="btn btn-light" style="color: maroon; border: 1px maroon solid;margin-top: 0px;"></input></p>
	</form>

@stop