
@extends('modif')

@section('content')


	<form method="post" action="{{route('postmodifAcc',['id'=>$m['id']])}}">
		{{csrf_field()}}
		<h4 class="bordeaux">Modification de l'onglet accueil</h4>
		<div class="ligne"></div>
	 	<p>Titre : <input class="form-control" type="text" name="titre" value="{{$m->TitreAcc}}" id="titre"/></p>
	 	<div class="ligne"></div>
	 	<p>Texte : <textarea class="form-control" type="text" name="texte" id="texte" style="height: 400px">{{$m->TexteAcc}}</textarea></p>
	 	<div class="ligne"></div>
	 	<p><input type="submit" value="Modifier" class="btn btn-light" style="color: maroon; border: 1px maroon solid;margin-top: 0px;"></input></p>

	</form>


@stop