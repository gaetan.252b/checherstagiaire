<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/test', 'AccueilController@test');

Route::get('/', 'AccueilController@accueil');

Route::get('/accueil', 'AccueilController@accueil');

Route::get('/industries', 'IndustriesController@Industries')->name ("industries");

Route::get('/sante', 'SanteController@Sante')->name ("sante");

Route::get('/art', 'ArtController@Art')->name ("art");

Route::get('/commerce', 'CommerceController@commerce')->name ("commerce");



Route::get('/deleteI/{id}',"IndustriesController@deleteI")->name ("deleteI")->middleware('auth');

Route::get('/deleteS/{id}',"SanteController@deleteS")->name ("deleteS")->middleware('auth');

Route::get('/deleteA/{id}',"ArtController@deleteA")->name ("deleteA")->middleware('auth');

Route::get('/deleteC/{id}',"CommerceController@deleteC")->name ("deleteC")->middleware('auth');

Route::get('/createI',"IndustriesController@createI")->name ("createI")->middleware('auth');

Route::get('/createS',"SanteController@createS")->name ("createS")->middleware('auth');

Route::get('/createA',"ArtController@createA")->name ("createA")->middleware('auth');

Route::get('/createC',"CommerceController@createC")->name ("createC")->middleware('auth');


Route::post('/validI',"IndustriesController@validI")->name ("validI")->middleware('auth');

Route::post('/validS',"SanteController@validS")->name ("validS")->middleware('auth');

Route::post('/validA',"ArtController@validA")->name ("validA")->middleware('auth');

Route::post('/validC',"CommerceController@validC")->name ("validC")->middleware('auth');


Route::get('/modifI/{id}','IndustriesController@modifI')->name ("modifI")->middleware('auth');

Route::post('/postmodifI/{id}',['uses'=>"IndustriesController@postmodifI"])->name ("postmodifI")->middleware('auth');

Route::get('/modifS/{id}','SanteController@modifS')->name ("modifS")->middleware('auth');

Route::post('/postmodifS/{id}',['uses'=>"SanteController@postmodifS"])->name ("postmodifS")->middleware('auth');

Route::get('/modifA/{id}','ArtController@modifA')->name ("modifA")->middleware('auth');

Route::post('/postmodifA/{id}',['uses'=>"ArtController@postmodifA"])->name ("postmodifA")->middleware('auth');

Route::get('/modifC/{id}','CommerceController@modifC')->name ("modifC")->middleware('auth');

Route::post('/postmodifC/{id}',['uses'=>"CommerceController@postmodifC"])->name ("postmodifC")->middleware('auth');




Route::get('/accueilModif', 'AccueilController@all')->middleware('auth');

Route::get('/industriesAdmin', 'IndustriesController@all')->name ("industriesAdmin")->middleware('auth');

Route::get('/santeAdmin', 'SanteController@all')->name ("santeAdmin")->middleware('auth');

Route::get('/artAdmin', 'ArtController@all')->name ("artAdmin")->middleware('auth');

Route::get('/commerceAdmin', 'CommerceController@all')->name ("commerceAdmin")->middleware('auth');


Route::post('/message', 'ContactController@message')->name('message');

Route::get('/ContactDo','ContactController@ContactDo')->name ("ContactDo");

Route::post('/messageDo', 'ContactController@messageDo')->name('messageDo');

Route::get('/Information', 'InformationController@Information')->name('Information');

Route::get('/DeleteTS/{id}', 'InformationController@DeleteTS')->name('DeleteTS');

Route::get('/Validinfo/{id}', 'InformationController@Validinfo')->name('Validinfo');

Route::post('/postmodifTS',['uses'=>"InformationController@postmodifTS"])->name ("postmodifTS");

Route::get('/pdf', 'InformationController@pdf')->name('pdf');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');


