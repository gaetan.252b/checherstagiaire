<?php

namespace App\Http\Controllers;
use App\Art;
use App\Image;
use Validator;
use Illuminate\Http\Request;

class ArtController extends Controller
{
    public function Art()
    {
        $tab=Art::all();
        return view('art')->with('tab',$tab);
    }

    public function all()
    {
        $tab=Art::all();
        return view('artAdmin')->with('tab',$tab);
    }

    public function deleteA($id){
        Art::destroy($id);
        return redirect ("artAdmin");
    }

    public function createA(){
        return view('artCreer');
    }

    public function validA(request $request){

        $validator = Validator::make($request->all(), [
        'nom' => 'required|max:255',
        'debut' => 'required|max:255',
        'fin' => 'required|max:255',
        'info' => 'required|max:255|min:3',
        ]);
            if ($validator->fails()) {
            return redirect()->route('artAdmin')->withErrors($validator)->withInput();
        }

        $c = new Art;
        $c->Nom=$request->get("nom");
        $c->Debut=$request->get("debut");
        $c->Fin=$request->get("fin");
        $c->Info=$request->get("info");
        $c->save();
        return redirect ("artAdmin");
    }

    public function modifA($id)
    {
        $m = Art::find($id);
    	return view('artModif')->with('m',$m);
    }
    public function postmodifA(request $request,$id)
    {
        $c = Art::find($id);
        $c->Nom=$request->get("nom");
        $c->Debut=$request->get("debut");
        $c->Fin=$request->get("fin");
        $c->Info=$request->get("info");
        $c->save();
        return redirect ("artAdmin");
    }
}
