<?php

namespace App\Http\Controllers;
use App\Information;
use App\Image;
use Validator;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    public function Information()
    {
        $tab=Information::all();
        return view('Information')->with('tab',$tab);
    }

    public function Validinfo($id)
    {
        $m=Information::find($id);
        return view('formulaire')->with('m',$m);
    }
    public function postmodifTS(request $request)
    {
		$c = new Information;
        $c->entreprise=$request->get("entreprise");
        $c->adresse=$request->get("adresse");
        $c->telephone=$request->get("telephone");
        $c->nom=$request->get("nom");
        $c->save();

        return view ("telecharger");
    }
}
