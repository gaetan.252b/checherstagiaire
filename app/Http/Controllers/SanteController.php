<?php

namespace App\Http\Controllers;
use App\Sante;
use App\Image;
use Validator;
use Illuminate\Http\Request;

class SanteController extends Controller
{
    public function Sante()
    {
        $tab=Sante::all();
        return view('sante')->with('tab',$tab);
    }

    public function all()
    {
        $tab=Sante::all();
        return view('santeAdmin')->with('tab',$tab);
    }

    public function deleteS($id){
        Sante::destroy($id);
        return redirect ("santeAdmin");
    }

    public function createS(){
        return view('santeCreer');
    }

    public function validS(request $request){

        $validator = Validator::make($request->all(), [
        'nom' => 'required|max:255',
        'debut' => 'required|max:255',
        'fin' => 'required|max:255',
        'info' => 'required|max:255|min:3',
        ]);
            if ($validator->fails()) {
            return redirect()->route('artAdmin')->withErrors($validator)->withInput();
        }

        $c = new Sante;
        $c->Nom=$request->get("nom");
        $c->Debut=$request->get("debut");
        $c->Fin=$request->get("fin");
        $c->Info=$request->get("info");
        $c->save();
        return redirect ("santeAdmin");
    }

    public function modifS($id)
    {
        $m = Sante::find($id);
    	return view('santeModif')->with('m',$m);
    }
    public function postmodifS(request $request,$id)
    {
        $c = Sante::find($id);
        $c->Nom=$request->get("nom");
        $c->Debut=$request->get("debut");
        $c->Fin=$request->get("fin");
        $c->Info=$request->get("info");
        $c->save();
        return redirect ("santeAdmin");
    }
}
