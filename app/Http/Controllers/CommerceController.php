<?php

namespace App\Http\Controllers;
use App\Commerce;
use App\Image;
use Validator;
use Illuminate\Http\Request;

class CommerceController extends Controller
{
    public function commerce()
    {
        $tab=Commerce::all();
        return view('commerce')->with('tab',$tab);
    }

    public function all()
    {
        $tab=Commerce::all();
        return view('commerceAdmin')->with('tab',$tab);
    }

    public function deleteC($id){
        Commerce::destroy($id);
        return redirect ("commerceAdmin");
    }

    public function createC(){
        return view('commerceCreer');
    }

    public function validC(request $request){

        $validator = Validator::make($request->all(), [
        'nom' => 'required|max:255',
        'debut' => 'required|max:255',
        'fin' => 'required|max:255',
        'info' => 'required|max:255|min:3',
        ]);
            if ($validator->fails()) {
            return redirect()->route('artAdmin')->withErrors($validator)->withInput();
        }

        $c = new Commerce;
        $c->Nom=$request->get("nom");
        $c->Debut=$request->get("debut");
        $c->Fin=$request->get("fin");
        $c->Info=$request->get("info");
        $c->save();
        return redirect ("commerceAdmin");
    }

    public function modifC($id)
    {
        $m = Commerce::find($id);
    	return view('commerceModif')->with('m',$m);
    }
    public function postmodifC(request $request,$id)
    {
        $c = Commerce::find($id);
        $c->Nom=$request->get("nom");
        $c->Debut=$request->get("debut");
        $c->Fin=$request->get("fin");
        $c->Info=$request->get("info");
        $c->save();
        return redirect ("commerceAdmin");
    }
}
