<?php

namespace App\Http\Controllers;
use App\Industries;
use App\Image;
use Validator;
use Illuminate\Http\Request;

class IndustriesController extends Controller
{
    public function Industries()
    {
        $tab=Industries::all();
        return view('industries')->with('tab',$tab);
    }

    public function all()
    {
        $tab=Industries::all();
        return view('industriesAdmin')->with('tab',$tab);
    }

    public function deleteI($id){
        Industries::destroy($id);
        return redirect ("industriesAdmin");
    }

    public function createI(){
        return view('industriesCreer');
    }

    public function validI(request $request){

        $validator = Validator::make($request->all(), [
        'nom' => 'required|max:255',
        'debut' => 'required|max:255',
        'fin' => 'required|max:255',
        'info' => 'required|max:255|min:3',
        ]);
            if ($validator->fails()) {
            return redirect()->route('artAdmin')->withErrors($validator)->withInput();
        }

        $c = new Industries;
        $c->Nom=$request->get("nom");
        $c->Debut=$request->get("debut");
        $c->Fin=$request->get("fin");
        $c->Info=$request->get("info");
        $c->save();
        return redirect ("industriesAdmin");
    }

    public function modifI($id)
    {
        $m = Industries::find($id);
    	return view('industriesModif')->with('m',$m);
    }
    public function postmodifI(request $request,$id)
    {
        $c = Industries::find($id);
        $c->Nom=$request->get("nom");
        $c->Debut=$request->get("debut");
        $c->Fin=$request->get("fin");
        $c->Info=$request->get("info");
        $c->save();
        return redirect ("industriesAdmin");
    }
}
